import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';
import {Dropdown} from 'primereact/dropdown';

import NbuCurrencyModel from '../../models/NbuCurrencyModel';

class ConverterPage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            nbuCurrencies: [],
            convForm: {
                from: new NbuCurrencyModel(1, 'Украинская гривна', 1, 'UAH', '1'),
                to: new NbuCurrencyModel(1, 'Украинская гривна', 1, 'UAH', '1'),
                toValue: 0,
                fromValue: 0
            }
        };
    }

    componentWillMount(){
        this.doRequest();
    }

    doRequest(){
        axios.get('http://exrates.greencomet.net/nbu')
            .then(response => {

                let currencies = response.data;
                currencies.push(new NbuCurrencyModel(1, 'Украинская гривна', 1, 'UAH', '1'));
                this.setState({nbuCurrencies: currencies});

            })
            .catch(error => {
                console.log(error);
            });
    }

    change(e, target = 'to'){

        let source = {
            to: 'from',
            from: 'to'
        };

        let result = (this.state.convForm.from.rate / this.state.convForm.to.rate) * this.state.convForm[source[target] + 'Value'];
        let convForm = {...this.state.convForm};

        convForm = convForm[target + 'Value'] = Math.floor(result * 100) / 100;
        //this.setState({convForm});

        console.log(convForm);

    }

    render() {
        return (
            <div className="ConverterPage">
                <h1>Конвертер валют</h1>
                <p>Источник данных НБУ</p>
                <div className="convert p-grid">
                    <div className="convert__l-bl p-md-6">
                        <h4>Отдаем</h4>
                        <div className="form-group">
                            <Dropdown dataKey="rate"
                                      value={this.state.convForm.from}
                                      options={this.state.nbuCurrencies}
                                      optionLabel="txt"
                                      filter={true}
                                      onChange={this.change.bind(this)}
                                      style={{width: '100%'}}
                                      placeholder="Select..."/>
                        </div>
                        <div className="form-group">
                            <InputText type="number" min="0" value={this.state.convForm.fromValue} onChange={this.change.bind(this)} />
                        </div>
                    </div>
                    <div className="convert__r-bl p-md-6">
                        <h4>Получаем</h4>
                        <div className="form-group">
                            <Dropdown dataKey="rate"
                                      value={this.state.convForm.to}
                                      options={this.state.nbuCurrencies}
                                      optionLabel="txt"
                                      filter={true}
                                      onChange={this.change.bind(this)}
                                      style={{width: '100%'}}
                                      placeholder="Select..."/>
                        </div>
                        <div className="form-group">

                        </div>
                     </div>
                </div>
            </div>
        );
    }
}

export default ConverterPage;