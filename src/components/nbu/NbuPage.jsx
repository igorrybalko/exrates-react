import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';

class NbuPage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            nbuCurrencies: [],
            filteredNbuCurrencies: []
        };
    }

    componentWillMount(){
        this.doRequest();
    }

    doRequest(){
        axios.get('http://exrates.greencomet.net/nbu')
            .then(response => {

                this.setState({nbuCurrencies: response.data});
                this.copyNbuCurrencies();

            })
            .catch(error => {
                console.log(error);
            });
    }

    copyNbuCurrencies() {
        this.setState({filteredNbuCurrencies: this.state.nbuCurrencies.slice()});
    }
    search(e){
        if (!e.target.value) {this.copyNbuCurrencies(); } // when nothing has typed
        let filteredNbuCurrencies = this.state.nbuCurrencies.slice().filter(
            item => item.txt.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1
        );

        this.setState({filteredNbuCurrencies})
    }
    render() {
        return (
            <div className='NbuPage'>
                <div className="p-datatable p-component">
                    <div className="p-datatable-wrapper">
                        <table>
                            <thead className="p-datatable-thead">
                            <tr>
                                <th colSpan="2">
                                    <div className="form-el">
                                        <InputText placeholder="Please input" onChange={this.search.bind(this)} />
                                    </div>
                                </th>
                                <th>Курс</th>
                            </tr>
                            </thead>
                            <tbody className="p-datatable-tbody">
                                {this.state.filteredNbuCurrencies.map((currency, index) => {
                                    return (<tr key={currency.cc}>
                                        <td className="nbu_id">{currency.cc}</td>
                                        <td className="cryptoex__title-td">
                                            <span className="nbu__title">{currency.txt}</span>
                                        </td>
                                        <td className="cryptoex__rate">
                                            <span>{currency.rate}</span>
                                        </td>
                                    </tr>)
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }
}

export default NbuPage;