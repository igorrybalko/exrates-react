import React from 'react';
import { NavLink } from "react-router-dom";

class AppHeaderMenu extends React.Component{

    constructor(props) {
        super(props);

        this.state = {};
        this.menuItems = [
            {"title":"\u041a\u0443\u0440\u0441 \u0432\u0430\u043b\u044e\u0442","url":"/","id":"1"},
            {"title":"\u041a\u0443\u0440\u0441 \u041d\u0411\u0423","url":"/nbu","id":"2"},
            {"title":"\u041a\u043e\u043d\u0432\u0435\u0440\u0442\u0435\u0440","url":"/converter","id":"4"},
            {"title":"\u041d\u043e\u0432\u043e\u0441\u0442\u0438","url":"/news","id":"5"},
            {"title":"\u041a\u0440\u0438\u043f\u0442\u043e\u0432\u0430\u043b\u044e\u0442\u044b","url":"/crypto","id":"3"}
        ];

    }

    openMenu(){

    }

    closeMenu(){

    }

    render() {
        return (
            <nav className="top-menu">
                <div className="top-menu__mob-button" onClick={this.openMenu.bind(this)}>
                    <span></span>
                </div>
                <ul>
                    {this.menuItems.map(item => {
                        return (
                            <li key={item.id} className="top-menu_item">
                                <NavLink activeClassName="active" onClick={this.closeMenu.bind(this)} to={item.url}>{item.title}</NavLink>
                            </li>
                            )
                        })
                    }
                </ul>
            </nav>
        );
    }
}

export default AppHeaderMenu;