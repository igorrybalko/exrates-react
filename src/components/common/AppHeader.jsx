import React from 'react';
import { Link } from "react-router-dom";

import AppHeaderMenu from './AppHeaderMenu';

class AppHeader extends React.Component{

    render() {
        return (
            <header className="header">
                <div className="header__top">
                    <div className="wrap">
                        <div className="container-fluid">
                            <div className="header__logo">
                                <Link to="/">Exrates</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header__bottom">
                    <div className="wrap">
                        <div className="container-fluid">
                            <AppHeaderMenu></AppHeaderMenu>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

export default AppHeader;