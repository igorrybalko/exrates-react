import React from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import * as moment from 'moment';

class NewsListPage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            itemsBlog: []
        };
    }

    componentWillMount(){
        this.doRequest();
    }

    doRequest(){
        axios.get('http://exrates.greencomet.net/news')
            .then(response => {

                this.setState({itemsBlog: response.data});

            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <div className="NewsListPage">
                <h1>Финансовые новости</h1>
                <div className="blog-list">
                    { this.state.itemsBlog.map(item => {

                    return (
                        <div key={item.id} className="blog-list__item">
                            <div className="blog-list__img">
                                <div className="blog-list__img-wr">
                                    <Link to={'/news/' + item.id}><img src={item.img} alt=""/></Link>
                                </div>
                            </div>
                            <div className="blog-list__txt">
                                <h2 className="blog-list__title">
                                    <Link to={'/news/' + item.id}>{item.title}</Link>
                                </h2>
                                <div className="blog-list__date">{moment(item.date).format('DD/MM/YYYY')}</div>
                                <div className="blog-list__desc">{item.description}</div>
                            </div>
                        </div>)
                    })}
                </div>
            </div>
        );
    }
}

export default NewsListPage;