import React from 'react';
import axios from 'axios';
import * as moment from 'moment';

class NewsItemPage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            itemBlog: {}
        };
    }

    componentWillMount(){
        this.doRequest();
    }

    doRequest(){
        axios.get('http://exrates.greencomet.net/news')
            .then(response => {

                let itemBlog = response.data[this.props.match.params.id];
                itemBlog.img = itemBlog.img.replace('190x120','1200x0');

                this.setState({itemBlog});

            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <div className="blog-item">
                <h1>{this.state.itemBlog.title}</h1>
                <div className="blog-item__img" style={{backgroundImage: 'url(' + this.state.itemBlog.img + ')'}}></div>
                <div className="blog-item__date">{moment(this.state.itemBlog.date).format('DD/MM/YYYY')}</div>
                <div dangerouslySetInnerHTML={{__html: this.state.itemBlog.fulltext}}></div>
            </div>
        );
    }
}

export default NewsItemPage;