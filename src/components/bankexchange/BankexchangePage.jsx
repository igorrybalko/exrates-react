import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';
import * as moment from 'moment';

class BankexchangePage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {

            banks: [],
            filteredBanks: [],
            date: '',
            askBid: {},
            searchValue: '',

            bankexShowCssClass: {
                usd: true,
                eur: false,
                rub: false
            },

            bankexSwitchEls: [
                {title: 'Доллар', id: 'usd'},
                {title: 'Евро', id: 'eur'},
                {title: 'Рубль', id: 'rub'}
            ]
        };
    }

    componentWillMount(){
        this.doRequest();
    }

    doRequest(){
        axios.get('http://exrates.greencomet.net/banks')
            .then(response => {

                this.setState( {banks: response.data.organizations});
                this.setState( {date: response.data.date});
                this.setState({askBid: this.getAskBid(this.state.banks)});
                this.copyBanks();

            })
            .catch(error => {
                console.log(error);
            });
    }

    copyBanks() {
        this.setState({filteredBanks: this.state.banks.slice()});
    }

    search(e){

        if (!e.target.value) {this.copyBanks(); } // when nothing has typed
        let filteredBanks = this.state.banks.slice().filter(
            item => item.title.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1
        );

        this.setState( {filteredBanks});

    }
    changeCurrency(id) {

        switch (id) {
            case 'eur':
                this.setState({bankexShowCssClass: {usd: false, eur: true, rub: false}});
                break;
            case 'rub':
                this.setState({bankexShowCssClass: {usd: false, eur: false, rub: true}});
                break;
            default:
                this.setState({bankexShowCssClass: {usd: true, eur: false, rub: false}});
        }

    }

    getAskBid(arr) {

        let eurBid = [],
            rubBid = [],
            usdBid = [],
            eurAsk = [],
            rubAsk = [],
            usdAsk = [];

        for (let i = 0 ; i < arr.length; i++) {
            if (arr[i].orgType === 1) {

                if (arr[i].currencies.EUR) {
                    eurBid.push({
                        id: arr[i].id,
                        bid: +arr[i].currencies.EUR.bid
                    });

                    eurAsk.push({
                        id: arr[i].id,
                        ask: +arr[i].currencies.EUR.ask
                    });
                }

                if(arr[i].currencies.USD){
                    usdBid.push({
                        id: arr[i].id,
                        bid: +arr[i].currencies.USD.bid
                    });

                    usdAsk.push({
                        id: arr[i].id,
                        ask: +arr[i].currencies.USD.ask
                    });
                }

                if(arr[i].currencies.RUB){
                    rubBid.push({
                        id: arr[i].id,
                        bid: +arr[i].currencies.RUB.bid
                    });

                    rubAsk.push({
                        id: arr[i].id,
                        ask: +arr[i].currencies.RUB.ask
                    });
                }
            }
        }

        rubBid.sort(this.arrSortBid);
        eurBid.sort(this.arrSortBid);
        usdBid.sort(this.arrSortBid);
        rubAsk.sort(this.arrSortAsk);
        eurAsk.sort(this.arrSortAsk);
        usdAsk.sort(this.arrSortAsk);

        let askBid = {
            usd: {
                minBid: usdBid.shift(),
                maxBid: usdBid.pop(),
                minAsk: usdAsk.shift(),
                maxAsk: usdAsk.pop(),
            },
            eur: {
                minBid: eurBid.shift(),
                maxBid: eurBid.pop(),
                minAsk: eurAsk.shift(),
                maxAsk: eurAsk.pop()
            },
            rub: {
                minBid: rubBid.shift(),
                maxBid: rubBid.pop(),
                minAsk: rubAsk.shift(),
                maxAsk: rubAsk.pop()
            },
        };

        return askBid;
    }

    arrSortBid(a, b) {
        if(a.bid > b.bid) return 1;
        if(a.bid < b.bid)  return -1;
        return 0;
    }

    arrSortAsk(a, b) {
        if(a.ask > b.ask) return 1;
        if(a.ask < b.ask)  return -1;
        return 0;
    }

    render() {
        return (
            <div className='HomePage'>
                <h1>Курс валют в банках</h1>
                <div className="bankex-switch">
                    <ul>
                        { this.state.bankexSwitchEls.map(el => {
                            return (
                            <li key={el.id} onClick={this.changeCurrency.bind(this, el.id)} className={this.state.bankexShowCssClass[el.id] ? 'active' : ''}>
                                {el.title} ({el.id})
                            </li>)
                        })}
                    </ul>
                </div>
            <div className="bankex-time">Время обновления {moment(this.state.date).format("DD/MM/YYYY - HH:mm")}</div>
                <div className="bankex">
                    <div className="p-datatable p-component">
                        <div className="p-datatable-wrapper">
                            <table className={`table ${this.state.bankexShowCssClass.usd ? 'bankex__sh-usd' : ''} ${this.state.bankexShowCssClass.eur ? 'bankex__sh-eur' : ''}  ${this.state.bankexShowCssClass.rub ? 'bankex__sh-rub' : ''}`}>
                                <thead className="p-datatable-thead">
                                    <tr>
                                        <th>
                                            <div className="form-el">
                                                <InputText placeholder="Поиск по названию" onChange={this.search.bind(this)} />
                                            </div>
                                        </th>
                                        <th>Покупка</th>
                                        <th>Продажа</th>
                                    </tr>
                                </thead>
                                <tbody className="p-datatable-tbody">
                                    { this.state.filteredBanks.map( bank => {
                                        return( bank.orgType === 1 ?
                                            <React.Fragment key={bank.id}>
                                                <tr>
                                                    <td className="bankex__title-td">
                                                        <span className={`bankex__logo bankex__logo_id_${bank.oldId}`}></span>
                                                        <span className="bankex__title">{bank.title}</span>
                                                    </td>
                                                    <td className="bankex__bid-td">
                                                        <div className={`bankex__rate bankex__usd ${bank.id === this.state.askBid.usd.maxBid.id ? 'bankex__maxbid' : ''} ${bank.id === this.state.askBid.usd.minBid.id ? 'bankex__minbid' : ''}`}>
                                                           { bank.currencies.USD ? parseFloat(bank.currencies.USD.bid).toFixed(2) : ''}
                                                        </div>
                                                            { bank.currencies.EUR ? (
                                                                <div
                                                                className={`bankex__rate bankex__eur ${bank.id === this.state.askBid.eur.maxBid.id ? 'bankex__maxbid' : ''} ${bank.id === this.state.askBid.eur.minBid.id ? 'bankex__minbid' : ''}`}>
                                                                {parseFloat(bank.currencies.EUR.bid).toFixed(2)}
                                                                </div> ) : null}
                                                            {bank.currencies.RUB ? (
                                                        <div className={`bankex__rate bankex__rub ${bank.id === this.state.askBid.rub.maxBid.id ? 'bankex__maxbid' : ''} ${bank.id === this.state.askBid.rub.minBid.id ? 'bankex__minbid' : ''}`}>
                                                            {bank.currencies.RUB ? parseFloat(bank.currencies.RUB.bid).toFixed(3) : ''}
                                                        </div>) : null}
                                                    </td>
                                                    <td className="bankex__ask-td">
                                                        {bank.currencies.USD ? (
                                                            <div
                                                                className={`bankex__rate bankex__usd ${bank.id === this.state.askBid.usd.maxAsk.id ? 'bankex__maxask' : ''} ${bank.id === this.state.askBid.usd.minAsk.id ? 'bankex__minask' : ''}`}>
                                                                {parseFloat(bank.currencies.USD.ask).toFixed(2)}
                                                            </div>) : null
                                                        }
                                                        { bank.currencies.EUR ? (
                                                            <div
                                                            className={`bankex__rate bankex__eur ${bank.id === this.state.askBid.eur.maxAsk.id ? 'bankex__maxask' : ''} ${bank.id === this.state.askBid.eur.minAsk.id ? 'bankex__minask' : ''}`}>
                                                            {parseFloat(bank.currencies.EUR.ask).toFixed(2)}
                                                            </div>) : null
                                                        }
                                                        {bank.currencies.RUB ? (
                                                            <div
                                                                className={`bankex__rate bankex__rub ${bank.id === this.state.askBid.rub.maxAsk.id ? 'bankex__maxask' : ''} ${bank.id === this.state.askBid.rub.minAsk.id ? 'bankex__minask' : ''}`}>
                                                                {parseFloat(bank.currencies.RUB.ask).toFixed(3)}
                                                            </div>) : null
                                                        }
                                                    </td>
                                                </tr>
                                            </React.Fragment> : null)
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BankexchangePage;