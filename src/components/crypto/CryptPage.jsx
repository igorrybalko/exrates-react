import React from 'react';
import axios from 'axios';
import {InputText} from 'primereact/inputtext';

class CryptPage extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            cryptoCurrencies: [],
            filteredCryptoCurrencies: []
        };
    }

    componentWillMount(){
        this.doRequest();
    }

    doRequest(){
        axios.get('http://exrates.greencomet.net/crypto')
            .then(response => {

                this.setState({cryptoCurrencies: response.data});
                this.copyCryptoCurrencies();

            })
            .catch(error => {
                console.log(error);
            });
    }

    copyCryptoCurrencies(){
        this.setState({filteredCryptoCurrencies: this.state.cryptoCurrencies.slice()});
    }

    search(e){

        if(!e.target.value) this.copyCryptoCurrencies(); // when nothing has typed
        let filteredCryptoCurrencies = this.state.cryptoCurrencies.slice().filter(
            item => item.name.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1
        );

        this.setState({filteredCryptoCurrencies});

    }
    render() {
        return (
            <div className='CryptPage'>
                <h1>Курс криптовалют</h1>
                <div className="cryptoex">
                    <div className="p-datatable p-component">
                        <div className="p-datatable-wrapper">
                            <table>
                                <thead className="p-datatable-thead">
                                    <tr>
                                        <th>
                                            <div className="form-el">
                                                <InputText placeholder="Поиск по названию" onChange={this.search.bind(this)} />
                                            </div>
                                        </th>
                                        <th>Цена в $</th>
                                        <th>За сутки</th>
                                    </tr>
                                </thead>
                                <tbody className="p-datatable-tbody">
                                    {this.state.filteredCryptoCurrencies.map(currency  => {
                                        return (<tr key={currency.id}>
                                            <td className="cryptoex__title-td">
                                                <span className={`cryptoex__logo cryptoex__logo_id_${currency.id}`}></span>
                                                <span className="cryptoex__title">{currency.name}</span>
                                            </td>
                                            <td className="cryptoex__rate">
                                                <span>{currency.price_usd}</span>
                                            </td>
                                            <td className={`cryptoex__diff ${currency.percent_change_24h<0 ? 'cryptoex__diff_minus' : '' }`}>
                                                <span>{currency.percent_change_24h}%</span>
                                            </td>
                                        </tr>)
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CryptPage;