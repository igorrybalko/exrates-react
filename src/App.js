import React, { Component } from 'react';
import RouterOutlet from './router/RouterOutlet';

import AppHeader from './components/common/AppHeader';

import './App.css';

class App extends Component {
  render() {
    return (
        <div className="globwrap">
          <AppHeader/>
            <main>
                <div className="wrap">
                    <div className="p-grid">
                        <div className="p-lg-9">
                            <RouterOutlet/>
                        </div>
                        <div className="p-lg-3">
                            <div className="sidebar">

                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    );
  }
}

export default App;
