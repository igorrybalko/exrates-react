import React from 'react';
import {Route, Switch} from 'react-router-dom';

import BankexchangePage from '../components/bankexchange/BankexchangePage';
import NbuPage from '../components/nbu/NbuPage';
import ConverterPage from '../components/nbu/ConverterPage';
import CryptPage from '../components/crypto/CryptPage';
import NewsListPage from '../components/news/NewsListPage';
import NewsItemPage from '../components/news/NewsItemPage';

export default function RouterOutlet(){
    return (
        <Switch>
            <Route exact path="/" component={BankexchangePage} />
            <Route exact path="/nbu" component={NbuPage} />
            <Route path="/converter" component={ConverterPage} />
            <Route path="/crypto" component={CryptPage} />
            <Route exact path="/news" component={NewsListPage} />
            <Route path="/news/:id" component={NewsItemPage} />
        </Switch>
    );
}