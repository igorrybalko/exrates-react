export default class NbuCurrencyModel{
    constructor(r030, txt, rate, cc, exchangedate){
        return{
            r030,
            txt,
            rate,
            cc,
            exchangedate
        }
    }
}